/*!
 * MSUBootstrap Gruntfile
 * Copyright 2014 Michigan State University.
 */

module.exports = function (grunt) {
  'use strict';

  // Force use of Unix newlines
  grunt.util.linefeed = '\n';

  var configBridge = grunt.file.readJSON('./bootstrap/grunt/configBridge.json', { encoding: 'utf8' });

  // Project configuration.
  grunt.initConfig({

    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    bootstrap: grunt.file.readJSON('bootstrap/package.json'),
    banner: '/*!\n' +
            ' * MSUBootstrap v<%= pkg.version %> (<%= pkg.homepage %>)\n' +
            ' * Copyright 2014-<%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
            ' * Based on Bootstrap <%= bootstrap.version %> (<%= bootstrap.homepage %>)\n' +
            ' */\n',

    // Task configuration.
    clean: {
      dist: 'dist'
    },

    less: {
      compileCore: {
        options: {
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: '<%= pkg.name %>.css.map',
          sourceMapFilename: 'dist/css/<%= pkg.name %>.css.map'
        },
        src: '<%= pkg.name %>.less',
        dest: 'dist/css/<%= pkg.name %>.css'
      }
    },

    usebanner: {
      options: {
        position: 'top',
        banner: '<%= banner %>'
      },
      files: {
        src: 'dist/css/*.css'
      }
    },

    autoprefixer: {
      options: {
        browsers: configBridge.config.autoprefixerBrowsers
      },
      core: {
        options: {
          map: true
        },
        src: 'dist/css/<%= pkg.name %>.css'
      }
    },

    csslint: {
      options: {
        csslintrc: 'bootstrap/less/.csslintrc'
      },
      dist: [
        'dist/css/bootstrap.css'
      ]
    },

    cssmin: {
      options: {
        // TODO: disable `zeroUnits` optimization once clean-css 3.2 is released
        //    and then simplify the fix for https://github.com/twbs/bootstrap/issues/14837 accordingly
        compatibility: 'ie8',
        keepSpecialComments: '*',
        advanced: false
      },
      minifyCore: {
        src: 'dist/css/<%= pkg.name %>.css',
        dest: 'dist/css/<%= pkg.name %>.min.css'
      }
    },

    csscomb: {
      options: {
        config: 'bootstrap/less/.csscomb.json'
      },
      dist: {
        expand: true,
        cwd: 'dist/css/',
        src: ['*.css', '!*.min.css'],
        dest: 'dist/css/'
      }
    },

    copy: {
      fonts: {
        expand: true,
        src: 'bootstrap/dist/fonts/*',
        dest: 'dist/fonts',
        flatten: true
      },
      js: {
        expand: true,
        src: 'bootstrap/dist/js/*',
        dest: 'dist/js',
        flatten: true
      },
      img: {
        expand: true,
        src: 'img/*',
        dest: 'dist',
      },
      html: {
        src: 'template.html',
        dest: 'dist/template.html'
      }
    },

    watch: {
      source: {
        files: ['*.html','*.less'],
        tasks: ['less', 'copy']
      }
    },
    
    connect: {
      server: {
        options: {
          livereload: true,
          base: '.',
          port: 1855,
          open: true
        }
      }
    }

  });

  // These plugins provide necessary tasks.
  require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});
  require('time-grunt')(grunt);

  // CSS distribution task.
  grunt.registerTask('less-compile', ['less:compileCore']);
  grunt.registerTask('dist-css', ['less-compile', 'usebanner', 'autoprefixer:core', 'csscomb:dist', 'cssmin:minifyCore']);

  // Full distribution task.
  grunt.registerTask('dist', ['clean:dist', 'dist-css', 'copy:fonts', 'copy:js', 'copy:img', 'copy:html']);

  // run local dev server
  grunt.registerTask('serve', ['dist','connect:server','watch']);
  
  // Default task.
  grunt.registerTask('default', ['dist']);
};